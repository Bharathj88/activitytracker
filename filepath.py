import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import os
import datetime
from activity import event
import sys
import socket
import ActivityTracker
import ActivityTrackerApi
isdebug = True
class MyHandler(FileSystemEventHandler):
        
    def on_modified(self,event):
        if event.is_directory == False :
            name = 'modified'
            description = event.src_path
            timenew = time.ctime(os.stat(event.src_path).st_mtime)
            eventobj.timestamp = timenew
            eventobj.eventdesc = description
            eventobj.eventname = name
            ActivityTracker.ActivityTracker.savelog(self,eventobj)       

    def eventbroadcast(self):
        host = socket.gethostname()  # as both code is running on same pc
        port = 5000  # socket server port number
        client_socket = socket.socket()  # instantiate
        client_socket.connect((host, port))  # connect to the server
        message = 'hi'
        while True:
            client_socket.send(message.encode())  # send message
            data = client_socket.recv(1024).decode()  # receive response
            print('Received from server: ' + str(data))  # show in terminal
        client_socket.close()  # close the connection



print('begin')
# directory = input("Enter Directory To Watch")
# print(str(directory))
# DIRECTORY_TO_WATCH = "D://Products//Cloobot"   
directory = input("Enter Directory : ")
event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, path=str(directory), recursive=True)

if isdebug == True :
    print('started')

observer.start()
eventobj = event.event()
ActivityTrackerApi.app.run(port='5002')
input()

try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    observer.stop()

observer.join()
