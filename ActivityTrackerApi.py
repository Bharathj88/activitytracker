from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify
from openpyxl import load_workbook
import os.path
import pandas as pd
import win32com.client as wincl
import win32api, win32con,win32gui
import ctypes

app = Flask(__name__)
api = Api(app)

@app.route('/')
def index():
    return "hello world!"

@app.route('/userlogs')
def get():
    df_excel = pd.read_excel('C://Logs//UserLog.xlsx')
    json = df_excel.to_json(orient='records')
    return {'logs': json }

@app.route('/getwindows')
def getwindows():
    EnumWindows = ctypes.windll.user32.EnumWindows
    EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
    GetWindowText = ctypes.windll.user32.GetWindowTextW
    GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
    IsWindowVisible = ctypes.windll.user32.IsWindowVisible
    titles = []
    def foreach_window(hwnd, lParam):
        if IsWindowVisible(hwnd):
            length = GetWindowTextLength(hwnd)        
            buff = ctypes.create_unicode_buffer(length + 1)
            GetWindowText(hwnd, buff, length + 1)
            titles.append(buff.value)
        return True
    EnumWindows(EnumWindowsProc(foreach_window), 0)
    return {'titles': dumps(titles) }
    
